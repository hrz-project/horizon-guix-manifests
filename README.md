# Description 

This repository stores manifests file passed to `guix pack` 
that builds reproducible squashfs archive passed to 
singularity shell or run.

# Usage

Add dependencies to `X.scm`; do not forget to include `bash` or singularity won't be able to run it.

Build dependency using 

```bash
make X.pack.location
```

`X.pack.location` contain file path to guix derivation that contains the pack.

Run with `singularity shell $(cat X.pack.location)`.

## build singularity image

There is a way to convert a squashfs formatted pack to a `singularity image format` (SIF) image, using the `Bootstrap: localimage`.
The `sing.def` singularity definition file provides a template for this.

Use `make X.sif` to build the singularity image.

# License

[![gpl-v3](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)
