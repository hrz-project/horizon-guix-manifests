;; depends on the 'horizon' channel; see 
;; https://gitlab.com/hrz-project/horizon-guix-channels

(specifications->manifest
 '("bash" "which" "coreutils" "pigz@2.4" "gzip" "bzip2"
   "bloocoo@1.0.7" "bwa-mem2" "megahit@1.2.9"
   "samtools-dedup" "busco-coverage"))
