
.PHONY: all
all: assembly.sif

%.sif: %.def
		sudo singularity build $@ $<

%.def: %.pack.location
	sed "s|@PACKLOC@|$$(cat $<)|" sing.def.in > $@

%.pack.location: %.scm
	guix pack -L ~/shared/recipes/recipes/ -m $< -f squashfs --entry-point=bin/sh -S /bin=bin > $@
